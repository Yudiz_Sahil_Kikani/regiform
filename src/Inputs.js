import React, { useState } from 'react'

const Inputs = () => {
  const [signin, setSignin] = useState({ Name: '', Email: '', Phonenumber: '', Address: '', Birthdate: '', Age: '' })
  const [logins, setLogins] = useState([])
  const enteries = (e) => {
    const name = e.target.name
    const value = e.target.value
    console.log(name, value)
    setSignin({ ...signin, [name]: value })
  }
  const handelentries = (e) => {
    e.preventDefault()
    const newLogins = { ...signin, id: new Date().getTime().toString() }
    setLogins([...logins, newLogins])
    console.log(logins)

    setSignin({ Name: '', Email: '', Phonenumber: '', Address: '', Birthdate: '', Age: '' })
  }
  return (
        <>
         <div className="container">
          <form className="main-container" action="">
             <h1>Registartion Form</h1>
            <div className="firstname" id="input">
                <label htmlFor="firstname">Name :-</label><br/>
                <input type="text" name="Name" placeholder="Enter Your Name"
                value={signin.Name}
                onChange={enteries}
                 autoComplete="off" ></input>
            </div>
                <div className="Email" id="input">
                <label htmlFor="Email">Email Id :-</label><br/>
                <input type="text" name="Email" placeholder="Enter your Email Id"
                value={signin.Email}
                onChange={enteries}
                ></input>
            </div>
            <div className="Phone Number" id="input">
                <label htmlFor="phone number">Phone Number :-</label><br/>
                <input type="text" name="Phonenumber" placeholder="Enter Your Number"
                value={signin.Phonenumber}
                onChange={enteries}
                ></input>
            </div>
             <div className="Address" id="input">
                <label htmlFor="Address">Address :-</label><br/>
                <textarea name="Address" style={{ margin: '10px', width: '300px', height: '80px' }} type="text" placeholder="Enter Your Address"
                value={signin.Address}
                onChange={enteries}
                ></textarea>
            </div>
            <div className="Birthdate" id="input">
                <label htmlFor="Birthdate">Birthdate :-</label><br/>
                <input type="date" name="Birthdate" placeholder="Enter your BOD"
                value={signin.Birthdate}
                onChange={enteries}
                ></input>
            </div>
            <div className="Age" id="input">
                <label htmlFor="Age">Age :-</label><br/>
                <input type="text" name="Age" placeholder="Enter Your Age"
                value={signin.Age}
                onChange={enteries}
                ></input>
            </div>
            <button type="submit" className="btn" onClick={handelentries}>Submit</button>
          </form>
            <div>
                {
                    logins.map((curInput) => {
                      return (
                            <div className="output" key={curInput.id}>
                                <h2>Name:- {curInput.Name}</h2>
                                <h2>Email:- {curInput.Email}</h2>
                                <h2>Phonenumber:- {curInput.Phonenumber}</h2>
                                <h2>Address:- {curInput.Address}</h2>
                                <h2>Birthdate:- {curInput.Birthdate}</h2>
                                <h2>Age;- {curInput.Age}</h2><br/>
                            </div>
                      )
                    })
                }
            </div>
            </div>
                   </>
  )
}

export default Inputs
